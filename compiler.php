<?php
// Criando arquivo Phar:
try {

    $phar = new Phar('assist.phar');

    // src/templates dir
    $phar->addFile("src/templates/blank.php");
    $phar->addFile("src/templates/controller.template.php");
    $phar->addFile("src/templates/htr_dot_json.template.php");
    $phar->addFile("src/templates/model.template.php");
    $phar->addFile("src/templates/validator.template.php");
    $phar->addFile("src/templates/view_index.template.php");
    $phar->addFile("src/templates/view_form_novo.template.php");
    $phar->addFile("src/templates/view_form_editar.template.php");
    // vendor/ dir
    $phar->addFile("vendor/autoload.php");
    $phar->addFile("vendor/composer/ClassLoader.php");
    $phar->addFile("vendor/composer/autoload_classmap.php");
    $phar->addFile("vendor/composer/autoload_namespaces.php");
    $phar->addFile("vendor/composer/autoload_psr4.php");
    $phar->addFile("vendor/composer/autoload_real.php");
    $phar->addFile("vendor/composer/autoload_static.php");
    $phar->addFile("vendor/composer/installed.json");
    // src/ dir
    $phar->addFile("src/Bootstrap.php");
    // src/Commands/ dir
    $phar->addFile("src/Commands/CommandAbstract.php");
    $phar->addFile("src/Commands/CommonTrait.php");
    $phar->addFile("src/Commands/Configdb.php");
    $phar->addFile("src/Commands/Configkey.php");
    $phar->addFile("src/Commands/Controller.php");
    $phar->addFile("src/Commands/Help.php");
    $phar->addFile("src/Commands/Model.php");
    $phar->addFile("src/Commands/Module.php");
    $phar->addFile("src/Commands/Validator.php");
    $phar->addFile("src/Commands/View.php");
    $phar->addFile("src/Commands/Vwindex.php");
    $phar->addFile("src/Commands/Vwinsert.php");
    $phar->addFile("src/Commands/Vwupdate.php");
    $phar->addFile("src/Commands/Welcome.php");
    // src/Config dir
    $phar->addFile("src/Config/app.php");
    // src/Databases dir
    $phar->addFile("src/Databases/SgbdAbstract.php");
    $phar->addFile("src/Databases/SgbdFactory.php");
    $phar->addFile("src/Databases/Mysql.php");
    // src/Exceptions dir
    $phar->addFile("src/Exceptions/BootstrapException.php");
    $phar->addFile("src/Exceptions/DatabaseException.php");
    // src/Interfaces dir
    $phar->addFile("src/Interfaces/CommandInterface.php");
    // src/Subcommands dir
    $phar->addFile("src/Subcommands/Configdb/HtrDotJson.php");
    $phar->addFile("src/Subcommands/Configdb/Mysql.php");
    $phar->addFile("src/Subcommands/Configdb/Sqlite.php");
    // src/System dir
    $phar->addFile("src/System/LoadJsonFile.php");
    // / dir
    $phar->addFile("index.php");

    $phar->setStub($phar->createDefaultStub('index.php'));
    
    rename('assist.phar', 'assist');
    
    print "Arquivo 'assist' criado com sucesso\n";
    
} catch (Exception $e) {
    
    print $e->getMessage() . "\n";
}
