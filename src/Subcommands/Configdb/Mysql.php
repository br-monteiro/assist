<?php

namespace Assist\Subcommands\Configdb;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;
use Assist\Subcommands\Configdb\HtrDotJson;

/**
 * Comando responsável por criar as configurações para Mysql
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Mysql extends CommandAbstract implements CommandInterface
{
    private $pathToConfig;
    private $htrDotJsonObject;

    public function __construct($params)
    {
        parent::__construct($params);
        $this->htrDotJsonObject = new HtrDotJson($params);

        $this->pathToConfig = getcwd() . '/App/Config/DatabaseConfig.php';
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    protected function rules()
    {
        if (array_key_exists('server', $this->params) === false) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer o servidor MySQL[end]\n"
            . "[\$ [green]php assist configdb mysql "
            . "--server=SERVIDOR "
            . "--dbname=DATABASE "
            . "--username=USUÁRIO "
            . "--password=SENHA[end]]");
        }

        if (array_key_exists('dbname', $this->params) === false) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer o nome do banco MySQL[end]\n"
            . "[\$ [green]php assist configdb mysql "
            . "--server={$this->params['server']} "
            . "--dbname=DATABASE "
            . "--username=USUÁRIO "
            . "--password=SENHA[end]]");
        }

        if (array_key_exists('username', $this->params) === false) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer o usuário do banco MySQL[end]\n"
            . "[\$ [green]php assist configdb mysql "
            . "--server={$this->params['server']} "
            . "--dbname={$this->params['dbname']} "
            . "--username=USUÁRIO "
            . "--password=SENHA[end]]");
        }

        if (array_key_exists('password', $this->params) === false) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer o usuário do banco MySQL[end]\n"
            . "[\$ [green]php assist configdb mysql "
            . "--server={$this->params['server']} "
            . "--dbname={$this->params['dbname']} "
            . "--username={$this->params['username']} "
            . "--password=SENHA[end]]");
        }
    }

    public function run()
    {
        $content = $this->prepareContent();
        if ($this->createFile($this->pathToConfig, $content)) {
            $this->showMsg("[green]Sucesso ao mudar as configurações de conexão com o [end][blue]MySQL[end]");

            $this->htrDotJsonObject->run();
        }
    }

    protected function prepareContent()
    {
        $content = "<?php\n";
        $content .= $this->getHeaderComment();
        $content .= "namespace App\Config;\n\n";
        $content .= "class DatabaseConfig\n";
        $content .= "{\n";
        $content .= "    public \$db = [\n";
        $content .= "        'sgbd' => 'mysql',\n";
        $content .= "        'server' => '" . base64_encode($this->params['server']) . "',\n";
        $content .= "        'dbname' => '" . base64_encode($this->params['dbname']) . "',\n";
        $content .= "        'username' => '" . base64_encode($this->params['username']) . "',\n";
        $content .= "        'password' => '" . base64_encode($this->params['password']) . "',\n";
        $content .= "        'options' => [\PDO::MYSQL_ATTR_INIT_COMMAND => \"SET NAMES 'utf8'\"]\n";
        $content .= "    ];\n";
        $content .= "}\n";

        return $content;
    }

    protected function getHeaderComment()
    {
        $header = "/**\n";
        $header .= " * @file DatabaseConfig.php\n";
        $header .= " * @version 0.2\n";
        $header .= " * - Class que configura as diretrizes para conexão com o Banco de Dados\n";
        $header .= " */\n\n";

        return $header;
    }

    public function getUses()
    {
        //todo
    }
}
