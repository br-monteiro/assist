<?php

namespace Assist\Subcommands\Configdb;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;

/**
 * Comando responsável por as configurações do arquivo htr.json
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class HtrDotJson extends CommandAbstract implements CommandInterface
{

    private $pathToConfig;

    public function __construct($params)
    {
        $this->registerCommand('getSgdb')
            ->registerCommand('getHostName')
            ->registerCommand('getDbName')
            ->registerCommand('getUserName')
            ->registerCommand('getPassword');

        parent::__construct($params);
        $this->pathToConfig = getcwd() . '/htr.json';
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/htr_dot_json.template.php';
    }

    protected function rules()
    {
        // todo
    }

    public function run()
    {
        if ($this->createFile($this->pathToConfig, $this->content())) {
            $this->showMsg("[green]Sucesso ao mudar as configurações do arquivo [end][blue]htr.json[end]");
        }
    }
    
    public function getSgdb()
    {
        return $this->params[1];
    }
    
    public function getHostName()
    {
        if (array_key_exists('server', $this->params) !== false) {
            return $this->params['server'];
        }

        return;
    }
    
    public function getDbName()
    {
        if (array_key_exists('dbname', $this->params) !== false) {
            return $this->params['dbname'];
        }

        return;
    }
    
    public function getUserName()
    {
        if (array_key_exists('username', $this->params) !== false) {
            return $this->params['username'];
        }

        return;
    }
    
    public function getPassword()
    {
        if (array_key_exists('password', $this->params) !== false) {
            return $this->params['password'];
        }

        return;
    }

    protected function getHeaderComment()
    {
        // todo
    }

    public function getUses()
    {
        //todo
    }
}
