<?php

namespace Assist;

use Assist\Exceptions\BootstrapException;

class Bootstrap
{
    /**
     * Recebe o array passdo por $argv do PHP
     * @var array
     */
    private $inputLine;

    /**
     * Armazena o comando a ser executado
     * @var string
     */
    private $command;

    /**
     * Armazena os parâmetros enviados para o comando
     * @var array
     */
    private $params;

    public function __construct($args = null)
    {
        $this->inputLine = $args;
        $this->makeCommand();
        $this->run();
    }

    /**
     * Construtor de comandos
     */
    private function makeCommand()
    {
        // elimina o primeiro elemento do array, pois não será usado na aplicação
        array_shift($this->inputLine);

        // recebe o nome do comando passado pelo prompt
        // caso nada seja passado, por default
        // executa o comando Welcome
        $this->command = isset($this->inputLine[0]) ? ucfirst($this->inputLine[0])  : "Welcome";

        // transforma em string o array passado por $this->inputLine
        $this->params = implode('@', $this->inputLine);

        // retira os caracteres especiais -- dos comandos
        $this->params = str_replace("--", "", $this->params);

        // transforma em array a string passada, quebrando as partes onde estiver o caractere @
        $this->params = explode("@", $this->params);
    }

    /**
     * Prepara a execução dos comandos passados para o assist
     * @throws BootstrapException
     */
    private function run()
    {
        // padrão do namespace para comandos
        $class_name = "\\Assist\\Commands\\" . $this->command;

        // verfica se existe o comando passo pelo prompt
        if (class_exists($class_name)) {

            // instancia a classe responsável pelo comando, passando os parâmetros no construtor
            $class_name = new $class_name($this->params);

            // verifica se a classe implementa a interface 
            // Interfaces\CommandInterface e se extende a 
            // classe Commands\CommandAbstract
            $interface = Interfaces\CommandInterface::class;
            $abstract = Commands\CommandAbstract::class;
            if (($class_name instanceof $interface) && ($class_name instanceof $abstract)) {

                // tenta executar o comando
                $class_name->run();
                // terminha a execução
                exit;
            }

        }

        // lança uma BootstrapException caso o comando não exista
        throw new BootstrapException("[red]{$this->command}[end]: Este não é um comando válido.\n"
                . "Para saber os comandos válidos digite:\n[\$ [green]php assist help[end]]");
    }
}