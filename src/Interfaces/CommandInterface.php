<?php

namespace Assist\Interfaces;

interface CommandInterface
{
    public function __construct($params);

    public function run();
}
