<?php

namespace Assist\Exceptions;

use Assist\Commands\CommonTrait;

class BootstrapException extends \Exception
{
    use CommonTrait;
    // Redefine a exceção de forma que a mensagem não seja opcional
    public function __construct($message, $code = 0, Exception $previous = null) {
        $this->show($message);
        // garante que tudo está corretamente inicializado
        parent::__construct($message, $code, $previous);
        exit;
    }

    // personaliza a apresentação do objeto como string
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function customFunction() {
        //echo "Uma função específica desse tipo de exceção\n";
    }
    
    private function show($mensagem)
    {
        $this->showMsg($mensagem);
    }
}
