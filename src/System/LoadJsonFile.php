<?php

namespace Assist\System;

use Assist\Exceptions\BootstrapException;

/**
 * Carrega o arquivo de configuração
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class LoadJsonFile
{

    private $defaultJsonFile;
    private $contentJson;

    public function __construct($jsonFile = null)
    {
        $this->setDefaultJsonFile(getcwd() . '/htr.json');
        if ($jsonFile) {
            $this->setDefaultJsonFile($jsonFile);
        }

        return $this->rules()
                ->loadFile()
                ->getJson();
    }

    public function getDefaultJsonFile()
    {
        return $this->defaultJsonFile;
    }

    public function setDefaultJsonFile($defaultJsonFile)
    {
        $this->defaultJsonFile = $defaultJsonFile;
        return $this;
    }

    /**
     * @return \stdClass Conteúdo decodificado do arquivo JSON
     */
    public function getJson()
    {
        return $this->contentJson;
    }

    /**
     * Regras de carregamento do Arquivo
     * @return \Assist\System\LoadJsonFile
     * @throws BootstrapException
     */
    private function rules()
    {
        // verifica se existe o arquivo de configuração hrt.json
        if (!file_exists($this->getDefaultJsonFile())) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]Não foi possível encontrar o arquivo de configuração.[end]\n"
            . "Path: [blue]" . $this->getDefaultJsonFile() . "[end]");
        }

        return $this;
    }

    /**
     * Carrega o arquivo JSON
     * @return \Assist\System\LoadJsonFile
     */
    private function loadFile()
    {
        $json = $this->getDefaultJsonFile();
        $json = file_get_contents($json);
        $this->contentJson = json_decode($json);

        return $this;
    }
}
