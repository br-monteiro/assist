<?php

namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por criar o arquivo index.blade.php usado como View
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Vwindex extends CommandAbstract implements CommandInterface
{

    public function __construct($params)
    {
        $this->registerCommand('makeTitleTable');
        $this->registerCommand('makeValuesTable');

        parent::__construct($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/view_index.template.php';
    }

    /**
     * Verifica as regras de execução do comando
     * @throws BootstrapException
     */
    protected function rules()
    {
        // verifica se foi passado um nome para o model
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!isset($this->params[1])) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer o nome do Controller vinculado à view.[end]\n"
            . "[\$ [green]php assist vwindex NOME-DO-CONTROLLER[end]]");
        }

        // verfica se foi passado o nome da entidade
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!$this->getEntity()) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer uma entidade para View.[end]\n"
            . "[\$ [green]php assist vwindex {$this->params[1]} --entity=NOME-DA-ENTIDADE[end]]");
        }
    }

    /**
     * Cria o arquivo
     * @throws BootstrapException
     */
    public function run()
    {
        $saveAs = $this->saveAs;

        if (!$saveAs) {
            $saveAs = getcwd() . "/App/Views/" . $this->getName() . "/";
        }

        $filename = $saveAs . "index.blade.php";

        if ($this->createPreview($filename, 'View: index')) {
            return true;
        }

        if (file_exists($filename) && !$this->isModeForce()) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]Já existe um arquivo[end] [blue]" . $filename . "[end]\n"
            . "[yellow]Para sobrescrever este arquivo use o modo forçado:[end]\n"
            . "[\$ [green]php assist vwindex " . $this->getName() . " [--force | --f][end]]");
        }

        // cria o diretório que conterá os arquivos de views
        $this->createDirectory($saveAs);

        // tenta criar um arquivo
        // caso true, exibe a mensagem de sucesso para o usuário
        // caso false, lança uma BootstrapException informando o erro.
        if ($this->createFile($filename, $this->content())) {
            $this->showMsg("[green]Sucesso ao criar o arquivo de View [end][blue]" . $filename . "[end]");
        }
    }

    /**
     * Retorna os comentários do início do arquivo
     * @return string
     */
    protected function getHeaderComment()
    {
        $value = "<!--\n";
        $value .= " * @view " . $this->getName() . "/index.blade.php\n";
        $value .= " * @created at " . $this->datetime() . "\n";
        $value .= " * - Criado Automaticamente pelo HTR Assist\n";
        $value .= " -->\n";
        return $value;
    }

    /**
     * Retorna as biblioteca usadas no Model
     * @return string
     */
    public function getUses()
    {
        // todo
    }

    public function makeTitleTable($modeReturn = [])
    {
        $pattern = null;

        if (array_key_exists('pattern', $modeReturn)) {
            $pattern = $modeReturn['pattern'];
        }

        $execute = function ($value) use ($pattern) {

            $field = $value['comment'] != '' ? $value['comment'] : $value['field'];

            if ($pattern) {
                $resultPattern = null;
                eval('$resultPattern = "' . $pattern . '";');
                return $resultPattern;
            }

            return "                            <th>" . $field . "</th>\n";
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }

    public function makeValuesTable($modeReturn = [])
    {
        $pattern = null;

        if (array_key_exists('pattern', $modeReturn)) {
            $pattern = $modeReturn['pattern'];
        }

        $execute = function ($value) use ($pattern) {

            $field = $value['field'];

            if ($pattern) {
                $resultPattern = null;
                eval('$resultPattern = "' . $pattern . '";');
                return $resultPattern;
            }

            return "                            <td>{{\$value['" . $field . "']}}</td>\n";
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }
}
