<?php
namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por criar os Validadores dos valores inseridos no banco do sistema
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Validator extends CommandAbstract implements CommandInterface
{
    public function __construct($params)
    {
        $this->registerCommand('makeValidates');

        parent::__construct($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/validator.template.php';
    }


    /**
     * Verifica as regras de execução do comando
     * @throws BootstrapException
     */
    protected function rules()
    {
        // verifica se foi passado um nome para o model
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!isset($this->params[1])) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer um nome para o Validador.[end]\n"
                . "[purple]Atenção: É importante que o nome do validador seja igual ao do Model que o usará.[end]\n"
                . "[\$ [green]php assist validator NOME-DO-VALIDADOR[end]]");
        }

        // verfica se foi passado o nome da entidade
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!$this->getEntity()) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer uma entidade para o Validador.[end]\n"
                . "[\$ [green]php assist validator {$this->params[1]} --entity=NOME-DA-ENTIDADE[end]]");
        }

    }

    /**
     * Cria o arquivo
     * @throws BootstrapException
     */
    public function run()
    {
        $saveAs = $this->saveAs;

        if (!$saveAs) {
            $saveAs  = getcwd() . '/App/Validators/';
        }

        $filename = $saveAs . $this->getName(['capitalize']) . "ValidatorTrait.php";

        if ($this->createPreview($filename, 'Validator')) {
            return true;
        }

        if (file_exists($filename) && !$this->isModeForce()) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]Já existe um arquivo[end] [blue]" . $filename . "[end]\n"
                . "[yellow]Para sobrescrever este arquivo use o modo forçado:[end]\n"
                . "[\$ [green]php assist validator " . $this->getName() . " [--force | --f][end]]");
        }

        // tenta criar um arquivo
        // caso true, exibe a mensagem de sucesso para o usuário
        // caso false, lança uma BootstrapException informando o erro.
        if ($this->createFile($filename, $this->content())) {
            $this->showMsg("[green]Sucesso ao criar o arquivo Validator [end][blue]" . $filename . "[end]");
        }
    }

    /**
     * Retorna os comentários do início do arquivo
     * @return string
     */
    protected function getHeaderComment()
    {
        $value = "/**\n";
        $value .= " * @validator " . $this->getName(['capitalize']) . "ValidatorTrait\n";
        $value .= " * @created at " . $this->datetime() . "\n";
        $value .= " * - Criado Automaticamente pelo HTR Assist\n";
        $value .= " */\n";
        return $value;
    }

    /**
     * Retorna as biblioteca usadas no Model
     * @return string
     */
    public function getUses()
    {
        $use = "use HTR\Helpers\Mensagem\Mensagem as msg;\n";
        $use .= "use Respect\Validation\Validator as v;\n";
        return $use;
    }

    /**
     * Retorna os metodos de validação
     * @param array $modeReturn
     */
    public function makeValidates($modeReturn = [])
    {
        $execute = function ($value) {

            $field = $value['comment'] != "" ? $value['comment'] : $value['field'];

            $nullable = !$value['is_null'] ? "notEmpty()->" : null;

            $is_null = $value['is_null'];

            $length = $value['length'] > 0 ? "length(1, " . $value['length'] . ")->" : null;

            if ($is_null) {
                $length = $value['length'] > 0 ? "length(null, " . $value['length'] . ")->" : null;
            }

            $typeValidate = "stringType()->" . $nullable . $length;

            if ($value['type'] == 'int') {
                $typeValidate = "intVal()->" . $nullable . $length;
            }

            if ($value['type'] == 'float') {
                $typeValidate = "floatVal()->" . $nullable . $length;
            }

            if ($value['type'] == 'text') {
                $typeValidate = "stringType()->" . $length;
            }

            $str = "    protected function validate" . $this->toCamelCase($value['field'], true) . "()\n";
            $str .= "    {\n";
            $str .= "        \$value = v::" . $typeValidate . "validate(\$this->get" . $this->toCamelCase($value['field'], true) . "());\n";
            $str .= "        if (!\$value) {\n";
            $str .= "            msg::showMsg('O campo " . $field . " deve ser preenchido corretamente.'\n";
            $str .= "                . '<script>focusOn(\"" . $value['field'] . "\");</script>', 'danger');\n";
            $str .= "            return \$this;\n";
            $str .= "        }\n";
            $str .= "    }\n\n";

            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }
}
