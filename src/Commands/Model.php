<?php
namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por criar os Models do sistema
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Model extends CommandAbstract implements CommandInterface
{
    public function __construct($params)
    {
        // registra os comandos específicos do Model
        $this->registerCommand('makeAttributes');
        $this->registerCommand('makeArrayDados');
        $this->registerCommand('makeNotDuplicate');
        $this->registerCommand('makeSetters');
        $this->registerCommand('makeCallValidates');

        parent::__construct($params);

    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/model.template.php';
    }

    /**
     * Verifica as regras de execução do comando
     * @throws BootstrapException
     */
    protected function rules()
    {
        // verifica se foi passado um nome para o model
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!isset($this->params[1])) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer um nome para o Model.[end]\n"
                . "[\$ [green]php assist model NOME-DO-MODEL[end]]");
        }

        // verfica se foi passado o nome da entidade
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!$this->getEntity()) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer uma entidade para o Model.[end]\n"
                . "[\$ [green]php assist model {$this->params[1]} --entity=NOME-DA-ENTIDADE[end]]");
        }

    }

    /**
     * Cria o arquivo
     * @throws BootstrapException
     */
    public function run()
    {
        $saveAs = $this->saveAs;

        if (!$saveAs) {
            $saveAs  = getcwd() . '/App/Models/';
        }

        $filename = $saveAs . $this->getName(['capitalize']) . "Model.php";

        if ($this->createPreview($filename, 'Model')) {
            return true;
        }

        if (file_exists($filename) && !$this->isModeForce()) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]Já existe um arquivo[end] [blue]" . $filename . "[end]\n"
                . "[yellow]Para sobrescrever este arquivo use o modo forçado:[end]\n"
                . "[\$ [green]php assist model " . $this->getName() . " [--force | --f][end]]");
        }

        // tenta criar um arquivo
        // caso true, exibe a mensagem de sucesso para o usuário
        // caso false, lança uma BootstrapException informando o erro.
        if ($this->createFile($filename, $this->content())) {
            $this->showMsg("[green]Sucesso ao criar o arquivo de Model [end][blue]" . $filename . "[end]");
        }
    }

    /**
     * Retorna os comentários do início do arquivo
     * @return string
     */
    protected function getHeaderComment()
    {
        $value = "/**\n";
        $value .= " * @model " . $this->getName(['capitalize']) . "Model\n";
        $value .= " * @created at " . $this->datetime() . "\n";
        $value .= " * - Criado Automaticamente pelo HTR Assist\n";
        $value .= " */\n";
        return $value;
    }

    /**
     * Retorna as biblioteca usadas no Model
     * @return string
     */
    public function getUses()
    {
        $use = "use HTR\System\ModelCRUD;\n";
        $use .= "use HTR\Helpers\Mensagem\Mensagem as msg;\n";
        $use .= "use HTR\Helpers\Paginator\Paginator;\n";
        $use .= "use HTR\System\Security;\n";
        return $use;
    }

    public function makeAttributes($modeReturn = [])
    {
        $executeString = function ($value) {
            return '    protected $' . $this->toCamelCase($value['field']) . ";\n";
        };

        return $this->makeForeachModeReturn($modeReturn, $executeString);
    }

    /**
     * Constroi o array usado nos metodos novo() e editar()
     * @param array $modeReturn
     * @return string
     */
    public function makeArrayDados($modeReturn = [])
    {
        $begin = '       $dados = [' . "\n";
        $end = "        ];\n";

        $executeString = function ($value) {
            return "          '" . $value['field'] . "' => \$this->get" . $this->toCamelCase($value['field'], true) . "(),\n";
        };

        return $this->makeForeachModeReturn($modeReturn, $executeString, $begin, $end);
    }

    public function makeNotDuplicate($modeReturn = [])
    {
        $begin = "    private function notDuplicate()\n";
        $begin .= "    {\n";

        $end = "    }\n";

        $execute = function ($value) {
            $field = $value['comment'] != "" ? $value['comment'] : $value['field'];

            $str = "        // Não deixa duplicar os valores do campo " . $value['field'] . "\n";
            $str .= "        \$this->db->instruction(new \HTR\Database\Instruction\Select(\$this->entidade))\n";
            $str .= "                ->setFields(['id'])\n";
            $str .= "                ->setFilters()\n";
            $str .= "                ->where('id', '!=', \$this->getId())\n";
            $str .= "                ->whereOperator('AND')\n";
            $str .= "                ->where('" . $value['field'] . "', '=' , \$this->get" . $this->toCamelCase($value['field'], true) . "());\n";
            $str .= "        \$result = \$this->db->execute()->fetch(\PDO::FETCH_ASSOC);\n\n";
            $str .= "        if (\$result) {\n";
            $str .= "            msg::showMsg('Já existe um registro com este(s) caractere(s) no campo '\n";
            $str .= "                . '<strong>" . $field . "</strong>.'\n";
            $str .= "                . '<script>focusOn(\"" . $value['field'] . "\")</script>', 'warning');\n";
            $str .= "        }\n";
            return $str;
        };
        return $this->makeForeachModeReturn($modeReturn, $execute, $begin, $end);
    }

    public function makeSetters($modeReturn = [])
    {
        $execute = function ($value) {
            $str = "        \$this->set" . $this->toCamelCase($value['field'], true) . "(filter_input(INPUT_POST, '" . $value['field'] . "'));\n";
            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }

    public function makeCallValidates($modeReturn = [])
    {
        $execute = function ($value) {
            $str = "        \$this->validate" . $this->toCamelCase($value['field'], true) . "();\n";
            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }
}
