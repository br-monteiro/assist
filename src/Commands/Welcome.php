<?php

namespace Assist\Commands;

use Assist\Commands\CommonTrait;
use Assist\Interfaces\CommandInterface;

class Welcome extends CommandAbstract implements CommandInterface
{
    use CommonTrait;
    
    protected function rules()
    {
        //todo
    }

    public function run(){
        $this->showMsg("[blue]Bem-vindo ao " . APP_NAME . " - " . APP_VERSION  . ". "
        . "Para saber mais digite:[end]\n"
        . "[\$ [green]php assist help[end]]");
        exit;
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    protected function getHeaderComment()
    {
        //todo
    }

    public function getUses()
    {
        //todo
    }
}
