<?php

namespace Assist\Commands;

trait CommonTrait
{

    private $colors = [
        'Linux' => [
            '[end]' => '$(tput sgr 0)',
            '[red]' => '$(tput setaf 1)',
            '[green]' => '$(tput setaf 2)',
            '[blue]' => '$(tput setaf 4)',
            '[yellow]' => '$(tput setaf 3)',
            '[cyan]' => '$(tput setaf 6)',
            '[white]' => '$(tput setaf 7)',
            '[purple]' => '$(tput setaf 5)'
        ],
        'WINNT' => [
            '[end]' => '',
            '[red]' => '',
            '[green]' => '',
            '[blue]' => '',
            '[yellow]' => '',
            '[cyan]' => '',
            '[white]' => '',
            '[purple]' => ''
        ],
        'Mac' => [
            '[end]' => '',
            '[red]' => '',
            '[green]' => '',
            '[blue]' => '',
            '[yellow]' => '',
            '[cyan]' => '',
            '[white]' => '',
            '[purple]' => ''
        ]
    ];

    private function colorMsg($mensagem)
    {
        foreach ($this->colors[PHP_OS] as $key => $value) {
            $mensagem = str_replace($key, $value, $mensagem);
        }
        return $mensagem;
    }
    
    protected function showMsg($mensagem)
    {
        // remove os caracteres especiais quando o OS for Microsoft Windows
        if (PHP_OS == "WINNT") {
            $mensagem = $this->colorMsg($mensagem);
            print $this->removeEspecialChar($mensagem) . "\n\n";
            return;
        }

        system("echo \"" . $this->colorMsg($mensagem). "\"");
    }

    protected function toCamelCase($str, $capitaliseFirstChar = false)
    {
        if ($capitaliseFirstChar) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $str);
    }
    
    protected function removeEspecialChar($str)
    {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[ÁÀÃÂÄ]/ui', 'A', $str);
        $str = preg_replace('/[ÉÈÊË]/ui', 'E', $str);
        $str = preg_replace('/[ÍÌÎÏ]/ui', 'I', $str);
        $str = preg_replace('/[ÓÒÕÔÖ]/ui', 'O', $str);
        $str = preg_replace('/[ÚÙÛÜ]/ui', 'U', $str);
        $str = preg_replace('/[Ç]/ui', 'C', $str);
        return $str;
    }
}
