<?php
namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por criar os Controllers do sistema
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Controller extends CommandAbstract implements CommandInterface
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/controller.template.php';
    }

    /**
     * Cria o arquivo
     * @throws BootstrapException
     */
    public function run()
    {
        $saveAs = $this->saveAs;

        if (!$saveAs) {
            $saveAs  = getcwd() . '/App/Controllers/';
        }

        $filename = $saveAs . $this->getName(['capitalize']) . "Controller.php";

        if ($this->createPreview($filename, 'Controller')) {
            return true;
        }

        if (file_exists($filename) && !$this->isModeForce()) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]Já existe um arquivo[end] [blue]" . $filename . "[end]\n"
                . "[yellow]Para sobrescrever este arquivo use o modo forçado:[end]\n"
                . "[\$ [green]php assist controller " . $this->getName() . " [--force | --f][end]]");
        }

        // tenta criar um arquivo
        // caso true, exibe a mensagem de sucesso para o usuário
        // caso false, lança uma BootstrapException informando o erro.
        if ($this->createFile($filename, $this->content())) {
            $this->showMsg("[green]Sucesso ao criar o arquivo de Controller [end][blue]" . $filename . "[end]");
        }
    }

    /**
     * Verifica as regras de execução do comando
     * @return \Assist\Commands\Controller
     * @throws BootstrapException
     */
    protected function rules()
    {
        // verifica se foi passado um nome para o controller
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!isset($this->params[1])) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer um nome para o Controller.[end]\n"
                . "[\$ [green]php assist controller NOME-DO-CONTROLLER[end]]");
        }

        // verfica se foi passado o nome da entidade
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!$this->getEntity() && (array_search('not-related', $this->params) === false)) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer uma entidade para o Controller.[end]\n"
                . "[\$ [green]php assist controller {$this->params[1]} --entity=NOME-DA-ENTIDADE[end]]");
        }

        return $this;
    }

    /**
     * Retorna os comentários do início do arquivo
     * @return string
     */
    protected function getHeaderComment()
    {
        $value = "/**\n";
        $value .= " * @Controller " . $this->getName(['capitalize']) . "Controller\n";
        $value .= " * @Created at " . $this->datetime() . "\n";
        $value .= " * - Criado Automaticamente pelo HTR Assist\n";
        $value .= " */\n";
        return $value;
    }

    /**
     * Retorna as biblioteca usadas no Controller
     * @return string
     */
    public function getUses()
    {
        $use = "use HTR\System\ControllerAbstract as Controller;\n";
        $use .= "use HTR\Interfaces\ControllerInterface;\n";
        $use .= "use HTR\Helpers\Access\Access;\n";
        $use .= "use App\Models\\" . $this->getName(['capitalize']) . "Model;\n";
        return $use;
    }
}
