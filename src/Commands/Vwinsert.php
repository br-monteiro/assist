<?php

namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por criar o arquivo form_novo.blade.php usado como View
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Vwinsert extends CommandAbstract implements CommandInterface
{

    public function __construct($params)
    {
        $this->registerCommand('makeInputLeft');
        $this->registerCommand('makeInputRight');

        parent::__construct($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/view_form_novo.template.php';
    }

    /**
     * Verifica as regras de execução do comando
     * @throws BootstrapException
     */
    protected function rules()
    {
        // verifica se foi passado um nome para o model
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!isset($this->params[1])) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer o nome do Controller vinculado à View.[end]\n"
            . "[\$ [green]php assist vwinsert NOME-DO-CONTROLLER[end]]");
        }

        // verfica se foi passado o nome da entidade
        // caso nenhum nome seja passado, lança uma BootstrapException
        if (!$this->getEntity()) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]É necessário fornecer uma entidade para View.[end]\n"
            . "[\$ [green]php assist vwinsert {$this->params[1]} --entity=NOME-DA-ENTIDADE[end]]");
        }
    }

    /**
     * Cria o arquivo
     * @throws BootstrapException
     */
    public function run()
    {
        $saveAs = $this->saveAs;

        if (!$saveAs) {
            $saveAs = getcwd() . "/App/Views/" . $this->getName() . "/";
        }

        $filename = $saveAs . "form_novo.blade.php";

        if ($this->createPreview($filename, 'View: form_novo')) {
            return true;
        }

        if (file_exists($filename) && !$this->isModeForce()) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]Já existe um arquivo[end] [blue]" . $filename . "[end]\n"
            . "[yellow]Para sobrescrever este arquivo use o modo forçado:[end]\n"
            . "[\$ [green]php assist vwinsert " . $this->getName() . " [--force | --f][end]]");
        }

        // cria o diretório que conterá os arquivos de views
        $this->createDirectory($saveAs);

        // tenta criar um arquivo
        // caso true, exibe a mensagem de sucesso para o usuário
        // caso false, lança uma BootstrapException informando o erro.
        if ($this->createFile($filename, $this->content())) {
            $this->showMsg("[green]Sucesso ao criar o arquivo de View [end][blue]" . $filename . "[end]");
        }
    }

    /**
     * Retorna os comentários do início do arquivo
     * @return string
     */
    protected function getHeaderComment()
    {
        $value = "<!--\n";
        $value .= " * @view " . $this->getName() . "/form_novo.blade.php\n";
        $value .= " * @created at " . $this->datetime() . "\n";
        $value .= " * - Criado Automaticamente pelo HTR Assist\n";
        $value .= " -->\n";
        return $value;
    }

    /**
     * Retorna as biblioteca usadas no Model
     * @return string
     */
    public function getUses()
    {
        // todo
    }

    public function makeInputLeft($modeReturn = [])
    {
        if (array_search('not-related', $this->params) === false) {
            return $this->makeInputRelatedLeft($modeReturn);
        }

        return $this->makeInputNotRelatedLeft($modeReturn);
    }

    public function makeInputRight($modeReturn = [])
    {
        if (array_search('not-related', $this->params) === false) {
            return $this->makeInputRelatedRight($modeReturn);
        }

        return $this->makeInputNotRelatedRight($modeReturn);
    }

    private function makeInputNotRelatedLeft($modeReturn = [])
    {

        $execute = function ($value, $key, $arrVal) use ($modeReturn) {

            $key++;
            // tamanho do array
            $arrLength = count($arrVal);
            $arrHalf = round($arrLength / 2);
            $str = null;
            $inputWithClass = array_key_exists('input-with-class', $modeReturn) ? $modeReturn['input-with-class'] : null;
            $divWithClass = array_key_exists('div-with-class', $modeReturn) ? $modeReturn['div-with-class'] : null ;

            if ($key <= $arrHalf) {

                $field = $value['comment'] != '' ? $value['comment'] : $this->toCamelCase($value['field'], true);
                $isNull = !$value['is_null'] ? "required" : null;
                $length = $value['length'] > 0 ? 'maxlength="' . $value['length'] . "\"\n" : null;

                $str = "                                <div class=\"" . $divWithClass . "\">\n";
                $str .= "                                    <label>" . $field . "</label>\n";
                $str .= "                                    <input type=\"text\"\n";
                $str .= "                                           id=\"" . $value['field'] . "\"\n";
                $str .= "                                           name=\"" . $value['field'] . "\"\n";
                $str .= "                                           placeholder=\"" . $field . "\"\n";
                $str .= "                                           class=\"" . $inputWithClass . "\"\n";
                $str .= "                                           " . $length;
                $str .= "                                           " . $isNull . ">\n";
                $str .= "                                </div>\n";
            }

            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }

    private function makeInputRelatedLeft($modeReturn = [])
    {

        $arrPattern = $this->mergeDescriptionAndRelation();

        $execute = function ($value, $key, $arrVal) use ($modeReturn) {

            $key++;
            // tamanho do array
            $arrLength = count($arrVal);
            $arrHalf = round($arrLength / 2);
            $str = null;
            $inputWithClass = array_key_exists('input-with-class', $modeReturn) ? $modeReturn['input-with-class'] : null;
            $divWithClass = array_key_exists('div-with-class', $modeReturn) ? $modeReturn['div-with-class'] : null;
            $field = $value['comment'] != '' ? $value['comment'] : $this->toCamelCase($value['field'], true);
            $isNull = !$value['is_null'] ? "required" : null;
            $length = $value['length'] > 0 ? 'maxlength="' . $value['length'] . "\"\n" : null;

            if ($key <= $arrHalf) {

                $str = "                                <div class=\"" . $divWithClass . "\">\n";
                $str .= "                                    <label>" . $field . "</label>\n";
                $str .= "                                    <input type=\"text\"\n";
                $str .= "                                           id=\"" . $value['field'] . "\"\n";
                $str .= "                                           name=\"" . $value['field'] . "\"\n";
                $str .= "                                           placeholder=\"" . $field . "\"\n";
                $str .= "                                           class=\"" . $inputWithClass . "\"\n";
                $str .= "                                           " . $length;
                $str .= "                                           " . $isNull . ">\n";
                $str .= "                                </div>\n";

                if (isset($value['reference_table'])) {

                    $str = "                                <div class=\"" . $divWithClass . "\">\n";
                    $str .= "                                    <label>" . $field . "</label>\n";
                    $str .= "                                    <select \n";
                    $str .= "                                           id=\"" . $value['field'] . "\"\n";
                    $str .= "                                           name=\"" . $value['field'] . "\"\n";
                    $str .= "                                           class=\"" . $inputWithClass . "\"\n";
                    $str .= "                                           " . $isNull . ">\n";
                    $str .= "                                           @foreach (\$result" . $this->toCamelCase($value['reference_table'], true) . " as \$value)\n";
                    $str .= "                                           <option value=\"{{\$value['" . $value['reference_field'] . "']}}\">\n";
                    $str .= "                                               {{\$value['" . $value['reference_field'] . "']}}\n";
                    $str .= "                                           </option>\n";
                    $str .= "                                           @endforeach\n";
                    $str .= "                                    </select>\n";
                    $str .= "                                </div>\n";
                }
            }

            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute, null, null, $arrPattern);
    }

    private function makeInputNotRelatedRight($modeReturn = [])
    {
        $execute = function ($value, $key, $arrVal) use ($modeReturn) {

            $key++;
            // tamanho do array
            $arrLength = count($arrVal);
            $arrHalf = round($arrLength / 2);
            $str = null;
            $inputWithClass = array_key_exists('input-with-class', $modeReturn) ? $modeReturn['input-with-class'] : null;
            $divWithClass = array_key_exists('div-with-class', $modeReturn) ? $modeReturn['div-with-class'] : null;

            if ($key > $arrHalf) {

                $field = $value['comment'] != '' ? $value['comment'] : $this->toCamelCase($value['field'], true);
                $isNull = !$value['is_null'] ? "required" : null;
                $length = $value['length'] > 0 ? 'maxlength="' . $value['length'] . "\"\n" : null;

                $str = "                                <div class=\"" . $divWithClass . "\">\n";
                $str .= "                                    <label>" . $field . "</label>\n";
                $str .= "                                    <input type=\"text\"\n";
                $str .= "                                           id=\"" . $value['field'] . "\"\n";
                $str .= "                                           name=\"" . $value['field'] . "\"\n";
                $str .= "                                           placeholder=\"" . $field . "\"\n";
                $str .= "                                           class=\"" . $inputWithClass . "\"\n";
                $str .= "                                           " . $length;
                $str .= "                                           " . $isNull . ">\n";
                $str .= "                                </div>\n";
            }

            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute);
    }

    private function makeInputRelatedRight($modeReturn = [])
    {

        $arrPattern = $this->mergeDescriptionAndRelation();

        $execute = function ($value, $key, $arrVal) use ($modeReturn) {

            $key++;
            // tamanho do array
            $arrLength = count($arrVal);
            $arrHalf = round($arrLength / 2);
            $str = null;
            $inputWithClass = array_key_exists('input-with-class', $modeReturn) ? $modeReturn['input-with-class'] : null;
            $divWithClass = array_key_exists('div-with-class', $modeReturn) ? $modeReturn['div-with-class'] : null;
            $field = $value['comment'] != '' ? $value['comment'] : $this->toCamelCase($value['field'], true);
            $isNull = !$value['is_null'] ? "required" : null;
            $length = $value['length'] > 0 ? 'maxlength="' . $value['length'] . "\"\n" : null;

            if ($key > $arrHalf) {

                $str = "                                <div class=\"" . $divWithClass . "\">\n";
                $str .= "                                    <label>" . $field . "</label>\n";
                $str .= "                                    <input type=\"text\"\n";
                $str .= "                                           id=\"" . $value['field'] . "\"\n";
                $str .= "                                           name=\"" . $value['field'] . "\"\n";
                $str .= "                                           placeholder=\"" . $field . "\"\n";
                $str .= "                                           class=\"" . $inputWithClass . "\"\n";
                $str .= "                                           " . $length;
                $str .= "                                           " . $isNull . ">\n";
                $str .= "                                </div>\n";

                if (isset($value['reference_table'])) {

                    $str = "                                <div class=\"" . $divWithClass . "\">\n";
                    $str .= "                                    <label>" . $field . "</label>\n";
                    $str .= "                                    <select \n";
                    $str .= "                                           id=\"" . $value['field'] . "\"\n";
                    $str .= "                                           name=\"" . $value['field'] . "\"\n";
                    $str .= "                                           class=\"" . $inputWithClass . "\"\n";
                    $str .= "                                           " . $isNull . ">\n";
                    $str .= "                                           @foreach (\$result" . $this->toCamelCase($value['reference_table'], true) . " as \$value)\n";
                    $str .= "                                           <option value=\"{{\$value['" . $value['reference_field'] . "']}}\">\n";
                    $str .= "                                               {{\$value['" . $value['reference_field'] . "']}}\n";
                    $str .= "                                           </option>\n";
                    $str .= "                                           @endforeach\n";
                    $str .= "                                    </select>\n";
                    $str .= "                                </div>\n";
                }
            }

            return $str;
        };

        return $this->makeForeachModeReturn($modeReturn, $execute, null, null, $arrPattern);
    }
}
