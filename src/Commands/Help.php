<?php

namespace Assist\Commands;

use Assist\Commands\CommonTrait;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

class Help extends CommandAbstract implements CommandInterface
{
    use CommonTrait;
    
    private $commandsDescriptio = [
                    // cria um novo módulo completo (MVC)
            'module' => [
                'desc' => "[yellow]Gera um módulo completo (MVC).\nÉ possível habilitar o modo forçado de criação de arquivo.[end]",
                'use' => '[$ php [blue]assist module NOME --entity=ENTIDADE[end] [green][--force | --f][end]]'
            ],
            // cria o arquivo de model
            'model' => [
                'desc' => "[yellow]Gera a Class responsável pelo Model.\nÉ possível habilitar o modo forçado de criação de arquivo.[end]",
                'use' => '[$ php [blue]assist model NOME --entity=ENTIDADE[end] [green][--force | --f][end]]'
            ],
            // cria o arquivo de view
            'view' => [
                'desc' => "[yellow]Gera os arquivos index.blade.php, form_novo.blade.php e form_editar.blade.php usados como Views.\nÉ possível habilitar o modo forçado de criação de arquivo.[end]",
                'use' => '[$ php [blue]assist view CONTROLLER --entity=ENTIDADE[end] [green][--force | --f][end]]'
            ],
            // cria o arquivo de controller
            'controller' => [
                'desc' => "[yellow]Gera a Class responsável pelo Controller.\nÉ possível habilitar o modo forçado de criação de arquivo.[end]",
                'use' => '[$ php [blue]assist controller NOME --entity=ENTIDADE[end] [green][--force | --f][end]]'
            ],
            // altera o arquivo de conexão com o banco de dados
            /*'configdb' => [
                'desc' => "[yellow]Configura os dados de conexão com o DB.[end]",
                'use' => "MySQL use [$ php [blue]assist configdb[end] [green]SERVER DATABASE USER PASS[end]]\n"
                        Sqlite use [$ php [blue]assist configdb[end] [green]NOME]"
            ],
            // altera o sal usado na criptografia
            'configkey' => [
                'desc' => "[yellow]Configura a string usada como sal na \ncriptografia de senhas e outros.[end]",
                'use' => '[$ php [blue]assist configkey[end]]'
            ],
            // adiciona um novo usuário administrador ao sistema
            'configadm' => [
                'desc' => "[yellow]Adiciona um novo usuário administrador ao sistema. (usuário temporário - usado apenas para logar)[end]",
                'use' => '[$ php [blue]assist configadm[end] [green]USUÁRIO SENHA[end]]'
            ],*/
            // assistente de ajuda ao usuário
            'help' => [
                'desc' => "[yellow]Este comando.[end]",
                'use' => '[$ php [blue]assist help[end] [green]COMANDO[end]]'
            ]
        ];
    
    public function __construct($params)
    {
        parent::__construct($params);
    }

    protected function rules()
    {
        //todo
    }

    public function run(){
        
        if ($this->getName()) {
            if (!array_key_exists($this->getName(), $this->commandsDescriptio)) {
                throw new BootstrapException("[red]Erro\n"
                    . $this->getName(['capitalize']) . ":[end] [yellow]Este comando não é válido.\n"
                    . "Consulte a documentação para mais detalhes.[end]");
            }
            
            $this->showMsg("[red]" . $this->getName() . "[end] -> "
                . $this->commandsDescriptio[$this->getName()]['desc'] . "\n"
                . $this->commandsDescriptio[$this->getName()]['use']);
            exit;
        }
        
        foreach ($this->commandsDescriptio as $key => $value) {

            $this->showMsg("[red]" . $key . "[end]\n"
                . $value['desc'] . "\n"
                . $value['use']);
        }
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    protected function getHeaderComment()
    {
        //todo
    }

    public function getUses()
    {
        //todo
    }
}
