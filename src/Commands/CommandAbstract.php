<?php

namespace Assist\Commands;

use Assist\Exceptions\BootstrapException;
use Assist\Commands\CommonTrait;
use Assist\System\LoadJsonFile;
use Assist\Databases\SgbdFactory;

abstract class CommandAbstract
{

    use CommonTrait;

    /**
     * Armazena o path do arquivo usado como template
     * @var string
     */
    protected $templateFile;

    /**
     * Conteúdo do template
     * @var string
     */
    protected $templateContent;

    /**
     * Armazena a string que será gravada no arquivo
     * @var string
     */
    protected $content;

    /**
     * Armazena o modo forçado de criação de arquivo. Por default, este modo vem desabilitado
     * @var bool
     */
    private $modeForce = false;

    /**
     * Registra os comandos
     * @var array
     */
    protected $commands = [
        // comandos comuns a todos os templates
        'getEntity', 'getHeaderComment', 'getName', 'getUses', 'datetime', 'getRelationModelName'
    ];

    /**
     * Parametros usados no comando
     * @var array
     */
    protected $params;

    /**
     * Recebe a instância da classe reponsável pelo SGBD escolhido
     * @var \Assist\Database\SgbdAbstract
     */
    private $database = false;

    /**
     * Recebe o path do arquivo a ser criado
     * @var string
     */
    protected $saveAs;

    /**
     * indica se o modo de preview está habilitado
     *
     * @var bool
     */
    protected $preview = false;

    /**
     * @var string entidade usada no comando
     */
    private $entity = false;

    public function __construct($params)
    {
        $this->setParams($params);
        $this->defaultTemplateFile();

        // prepara os parâmetros para o comando
        $this->prepareParams();
        // vefirica as regras de execução
        $this->rules();
        // carrega o arquivo usado como template
        $this->loadTemplate();
        // prepara o conteúdo que será gravado no arquivo
        $this->prepareContent();
    }

    /**
     * Informa o arquivo de template padrão
     */
    abstract protected function defaultTemplateFile();

    /**
     * regras implentadas pelos comandos
     */
    abstract protected function rules();

    /**
     * retornas as
     */
    abstract public function getUses();

    /**
     * Retorna os comentários do início do arquivo
     * @return string
     */
    abstract protected function getHeaderComment();

    /**
     * Retorna a instância do SGBD desejado
     * @return \Assist\Databases\SgbdFactory
     */
    protected function getSgbdInstance()
    {
        // verifica se é a primeira chamada do metodo,
        // caso true, cria uma nova instância de SgdbFactory e retorna essa instância
        // caso false, retorna a instância já criada em outra chamada do metodo
        if (!$this->database) {
            $sgbdfactory = new SgbdFactory();
            $this->database = $sgbdfactory->getSgbd();
        }

        return $this->database;
    }

    /**
     * Prepara os parametros usados no comando
     * @return \Assist\Commands\CommandAbstract
     */
    protected function prepareParams()
    {
        $params = [];

        foreach ($this->params as $value) {

            if (strpos($value, "=")) {

                $temp = explode("=", $value);
                $params[$temp[0]] = $temp[1];
                continue;
            }

            $params[] = $value;
        }

        // seta a entidade
        if (array_key_exists('entity', $params)) {
            $this->entity = $params['entity'];
        }

        // seta o modo forçado de criação de arquivo
        if (array_search('force', $params) || array_search('f', $params)) {
            // habilita o modo forçado de sobrescrita de arquivo
            $this->modeForce = true;
        }

        // seta o local onde o arquivo será salvo
        if (array_key_exists('save-as', $params)) {
            $this->saveAs = $params['save-as'];
        }

        // seta o modo de preview de criação dos arquivos
        if (array_search('preview', $params)) {
            // habilita o modo de preview
            $this->preview = true;
        }

        $this->params = $params;

        return $this;
    }

    /**
     * Seta os parametros usados nos comandos
     * @param array $params
     * @return \Assist\Commands\CommandAbstract
     */
    public function setParams($params = [])
    {
        $this->params = $params;
        return $this;
    }

    /**
     * Retorna o status do modo forçado de criação de arquivo
     * @return bool
     */
    protected function isModeForce()
    {
        return $this->modeForce;
    }

    /**
     * Cria um arquivo
     * @param type $filePath Caminho completo do arquivo
     * @param type $content Conteúdo a ser gravado dentro do arquivo
     * @return bool
     * @throws BootstrapException
     */
    protected function createFile($filePath, $content, $modeCreate = "w")
    {
        // abre/cria um arquivo somente para escrita
        $file = fopen($filePath, $modeCreate);

        if (!$file) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]Não foi possível criar o arquivo[end] [blue]" . $filePath . "[end]\n"
                . "Verifique sua permissão de usuário.");
        }
        // escre no arquivo criado
        fwrite($file, $content);
        // fecha o arquivo
        return fclose($file);
    }

    /**
     * Cria um novo diretório
     * @param string $path
     */
    protected function createDirectory($path)
    {
        if (!file_exists($path)) {
            @mkdir($path, 0777);
        }
    }
    /**
     * Prepara o conteúdo a ser gravado no arquivo
     * @return \Assist\Commands\CommandAbstract
     * @throws BootstrapException
     */
    protected function prepareContent()
    {
        // armazena o conteúdo
        $this->content = null;

        /**
         * percorre o conteúdo do template usado
         */
        foreach ($this->templateContent as $value) {

            // comandos e parametros usados dentro do template
            $command = null;
            $params = [];

            // armazena os caracteres encontrados antes de um comando de template
            $inFrontOfTheCommand = null;

            // verifica se a linha possui comando de template
            if (strstr($value, '[!]')) {
                $value = explode('[!]', $value);
                $inFrontOfTheCommand = $value[0];
                $command = $value[1];
                $command = rtrim($command);
            }

            // verifica se o comando possui parâmetros
            if (strstr($command, '<=>')) {
                $params = explode('<=>', $value[1]);
                $command = $params[0];
                $command = rtrim($command);

                // atribui a variável params o valor de dos parâmetros passados
                eval('$params = ' . $params[1] . ';');
            }

            // verifica se comando é válido
            if (($command) && array_search($command, $this->commands) === false) {

                throw new BootstrapException("[red]Erro\n"
                    . $command . ":[end] [yellow]Este comando não é válido.\n"
                    . "Consulte a documentação para mais detalhes.[end]\n"
                    . "Template: [blue]" . $this->templateFile . "[end]");
            }

            // se houver um comando executa o método responsável pelo comando
            if ($command) {
                $resultCommand = $this->$command($params);
                $value = $inFrontOfTheCommand . $resultCommand;
            }

            // concatena o resultado ao conteúdo abstraído pelo template
            $this->content .= $value;
        }

        return $this;
    }

    /**
     * retorna o conteúdo do arquivo a ser criado
     * @return string
     */
    protected function content()
    {
        return $this->content;
    }

    /**
     * Carrega o arquivo de template usado para o comando
     * @return \Assist\Commands\CommandAbstract
     * @throws BootstrapException
     */
    protected function loadTemplate()
    {
        $jsonFile = null;
        $commandName = $this->params[0];

        if (array_key_exists('use-json', $this->params)) {
            $jsonFile = $this->params['use-json'];
        }

        $json = new LoadJsonFile($jsonFile);
        $json = $json->getJson();
        $json = isset($json->commands->$commandName) ? $json->commands->$commandName : null;


        $template = isset($json->template) ? $json->template : $this->templateFile;

        $template = isset($this->params['template']) ? $this->params['template'] : $template;

        if (!file_exists($template)) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]O arquivo indicado para template não existe.[end]\n"
            . "Path: [blue]" . $template . "[end]");
        }

        if (!$this->saveAs && isset($json->saveas)) {

            $this->saveAs = $json->saveas . '/';
        }

        if (!file_exists($this->saveAs) && $this->saveAs != null) {
            throw new BootstrapException("[red]Erro[end]\n"
            . "[yellow]O path indicado para criação do arquivo não existe.[end]\n"
            . "Path: [blue]" . $template . "[end]");
        }

        // seta para templateContent o conteúdo do arquivo indicado para template
        $this->templateContent = file($template, FILE_USE_INCLUDE_PATH);

        return $this;
    }

    /**
     * Registra um novo comando a ser usado no template
     * @param type $command
     * @return \Assist\Commands\CommandAbstract
     */
    protected function registerCommand($command)
    {
        array_push($this->commands, $command);
        return $this;
    }

    /**
     * Retorna o modo como os dados serão retornados para o template
     * @param array $modeReturn
     * @return bool|array
     * @throws BootstrapException
     */
    protected function abstractModeReturn($modeReturn) {

        $arrReturn = false;

        // verifica se o
        if (isset($modeReturn['return'])) {

            if (isset($modeReturn['return']['only'])) {
                $mode = 'only';
                $arrReturn = $modeReturn['return']['only'];

                return [$mode, $arrReturn];
            }

            if (isset($modeReturn['return']['except'])) {
                $mode = 'except';
                $arrReturn = $modeReturn['return']['except'];

                return [$mode, $arrReturn];
            }

            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]Não foi especificado um modo suportado de retorno dos dados[end]\n"
                . "Template: [blue]" . $this->templateFile . "[end]");
        }

        return $arrReturn;
    }

    /**
     * Percorre o array retornado pelo metodo getDescriptionTable e abstrai
     * os dados de acordo com o mode de retorno fornecido em $modeReturn
     * @param array $modeReturn Mode de retorno dos dados. [only|except][array]
     * @param \Assist\Commands\callable $execute Callback passao para processar a string de retorno em cada iteração do laço
     * @param string $begin Valor concatenados no começo da string
     * @param string $end Valor concatenado no final da string
     * @param array $arrPattern Array usado alternativamente ao retornado por getDescriptionTable()
     * @return string
     * @throws BootstrapException
     */
    protected function makeForeachModeReturn($modeReturn, callable $execute, $begin = null, $end = null, array $arrPattern = null) {

        if (!is_callable($execute)) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer um Callback válido para o parametro \$execute do metodo makeForeachModeReturn()[end]");
        }

        // verifica se será usado um array diferente do retornado por getDescriptioTable
        if (!$arrPattern) {
            // cria uma nova instância do SGBD
            $database = $this->getSgbdInstance();
            // seta a entidade que será processada pelo SGBD
            $database->setEntity($this->getEntity());
            // processa e abstrai os dados da tabela
            $database->makeDescription();
            // abstrai os dados da tabela
            $arrPattern = $database->getDescriptionTable();
        }

        // checa qual o modo de retorno será usado
        // only, except ou nenhum (false)
        $modeReturn = $this->abstractModeReturn($modeReturn);

        $arrDados = $begin;

        // modo de retorno padrão
        if (!$modeReturn) {
            // percorre o array, executa o callback e concatena com o valor
            // retornado do callback
            foreach ($arrPattern as $key => $value) {
                $arrDados .= $execute($value, $key, $arrPattern);
            }

            $arrDados .= $end;
            return $arrDados;
        }

        // modo de retorno only
        if ($modeReturn[0] == 'only') {
            // percorre o array, executa o callback e concatena com o valor
            // retornado do callback
            foreach ($arrPattern as $key => $value) {
                if (array_search($value['field'], $modeReturn[1]) !== false) {
                    $arrDados .= $execute($value, $key, $arrPattern);
                }
            }

            $arrDados .= $end;
            return $arrDados;
        }

        // modo de retorno except
        if ($modeReturn[0] == 'except') {
            // percorre o array, executa o callback e concatena com o valor
            // retornado do callback
            foreach ($arrPattern as $key => $value) {
                if (array_search($value['field'], $modeReturn[1]) !== false) {
                    continue;
                }
                $arrDados .= $execute($value, $key, $arrPattern);
            }

            $arrDados .= $end;
            return $arrDados;
        }
    }

    /**
     * Obtem a saida para um arquivo html com highlight_string
     * @param string $filename Path do arquivo que seria criado
     * @return boolean
     */
    protected function createPreview($filepath, $filename)
    {
        if ($this->preview) {

            $content = "<meta charset='utf-8'>";
            $content .= "================================<br>";
            $content .= "Conteúdo do aqruivo <strong>" . $filepath .  "</strong><br><br>";
            $content .=  highlight_string($this->content(), true);

            $filepath = getcwd() . '/preview.html';

            $this->createFile($filepath, $content, 'a');

            $this->showMsg("[green]Sucesso ao incluir o resultado de saida do [yellow]{$filename}[end] para "
                . "o arquivo:[end] [blue]" . $filepath . "[end]");

            return true;
        }

        return false;
    }

    /**
     * Une o array de retornado por
     * getDescriptionTable() com o retornado por getReferenceTable()
     * @return array
     */
    protected function mergeDescriptionAndRelation()
    {
        // cria uma nova instância do SGBD
        $database = $this->getSgbdInstance();
        // seta a entidade que será processada pelo SGBD
        $database->setEntity($this->getEntity());
        // processa e abstrai os relações da tabela
        $database->makeDescription();
        $database->makeReference();

        // array retornado das relações das tabelas
        $arrReference = $database->getReferenceTable();
        // array retornado das caracteristicas da tabela
        $arrDescription = $database->getDescriptionTable();

        // array agregador de valores
        $arr = [];
        // variável auxiliar
        $aux = null;

        // percorre o array de caracteristicas
        foreach ($arrDescription as $keyDescription => $valueDescription) {

            // zera o valor antido da variável auxiliar
            $aux = null;

            // percorre o array de relações
            foreach ($arrReference as $keyReference => $valueReference) {
                // verifica se o campo é uma chave estrangeira
                if ($valueDescription['field'] == $valueReference['field']) {
                    // agrega características com as relações
                    $aux = array_merge($arrDescription[$keyDescription], $arrReference[$keyReference]);
                }
            }

            // armazena o processo no array agragador
            $arr[] = $aux ? : $arrDescription[$keyDescription];
        }

        // retorna o novo array
        return $arr;
    }

    ///////////
    ///// comandos disponíveis também para os templates
    ///////////

    /**
     * Retorna o nome da entidade
     * @return string
     */
    protected function getEntity()
    {
        return $this->entity;
    }

    /**
     * Retorna a data/hora atual do sistema
     * @param type $format Formato de retorno da data/hora, baseado em date() do PHP
     * @return string
     */
    protected function datetime($format = ['d-m-Y H:i:s'])
    {
        return date($format[0]);
    }

    /**
     * Retorna o nome dado ao arquivo/class
     * @param type $format
     * @return sring
     */
    protected function getName($format = [])
    {
        // transforma a string para o formato capitalize. Ex.: teste => Teste
        if (array_search('capitalize', $format) !== false) {
            return ucfirst($this->params[1]);
        }

        return isset($this->params[1]) ? $this->params[1] : null;
    }

    protected function getRelationModelName($modeReturn = [])
    {
        if (array_search('not-related', $this->params)) {
            return;
        }

        $execute = function ($value) {

            if (!isset($value['reference_table'])) {
                return;
            }

            $str = "        // relação com model " . $this->toCamelCase($value['reference_table'], true) . "Model\n";
            $str .= "        \$" . $this->toCamelCase($value['reference_table']) . "Model ="
                . " new \App\Models\\" . $this->toCamelCase($value['reference_table'], true) . "Model(\$this->access->pdo);\n";

            $str .= "        \$this->view['result" . $this->toCamelCase($value['reference_table'], true) . "'] ="
                . " \$" . $this->toCamelCase($value['reference_table']) . "Model->returnAll();\n";

            return $str;
        };
        // cria uma nova instância do SGBD
        $database = $this->getSgbdInstance();
        // seta a entidade que será processada pelo SGBD
        $database->setEntity($this->getEntity());
        // processa e abstrai os relações da tabela
        $database->makeReference();
        $arrPattern = $database->getReferenceTable();

        return $this->makeForeachModeReturn($modeReturn, $execute, null, null, $arrPattern);
    }
}
