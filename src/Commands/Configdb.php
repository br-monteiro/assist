<?php
namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por chamar os subcomandos criadores do arquivo de conexão com um SGBD
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Configdb extends CommandAbstract implements CommandInterface
{
    private $paramsToSubcommand;
    private $instanceOfSubcommand;

    public function __construct($params)
    {
        $this->paramsToSubcommand = $params;
        parent::__construct($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    protected function rules()
    {
        if (!$this->getName()) {
            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]É necessário fornecer o nome do SGBD que deseja configurar.[end]\n"
                . "[\$ [green]php assist configdb NOME-DO-SGBD[end]]");
        }

        $className = '\Assist\Subcommands\Configdb\\' . ucfirst(strtolower($this->getName()));

        // verfica se existe o comando passo pelo prompt
        if (class_exists($className)) {

            // instancia a classe responsável pelo comando, passando os parâmetros no construtor
            $className = new $className($this->paramsToSubcommand);

            // verifica se a classe implementa a interface
            // Interfaces\CommandInterface e se extende a
            // classe Commands\CommandAbstract
            $interface = CommandInterface::class;
            $abstract = CommandAbstract::class;

            if (($className instanceof $interface) && ($className instanceof $abstract)) {

                $this->instanceOfSubcommand = $className;
                return;
            }

        }

        throw new BootstrapException("[red]Erro\n"
                . $this->getName() . "[end] [yellow]Não é um SGBD suportado pelo sistema.\n"
                . "Consulte a documentação para mais detalhes.[end]");

    }

    public function run()
    {
        $this->instanceOfSubcommand->run();
    }

    public function getUses()
    {
        //todo
    }

    protected function getHeaderComment()
    {
        //todo
    }
}
