<?php

namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Commands\Vwindex;
use Assist\Commands\Vwinsert;
use Assist\Commands\Vwupdate;

/**
 * Comando responsável por criar os módulos do sistema (MVC)
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class View extends CommandAbstract implements CommandInterface
{

    private $indexView;
    private $insertView;
    private $updateView;

    public function __construct($params)
    {
        parent::__construct($params);

        $params[0] = 'vwindex';
        $this->indexView = new Vwindex($params);

        $params[0] = 'vwinsert';
        $this->insertView = new Vwinsert($params);

        $params[0] = 'vwupdate';
        $this->updateView = new Vwupdate($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    public function run()
    {
        $this->indexView->run();
        $this->insertView->run();
        $this->updateView->run();
    }

    protected function rules()
    {
        // todo
    }

    protected function getHeaderComment()
    {
        // todo
    }

    public function getUses()
    {
        // todo
    }
}
