<?php
namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Exceptions\BootstrapException;

/**
 * Comando responsável por criar novas chaves para o sistema
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Configkey extends CommandAbstract implements CommandInterface
{
    private $hashKey;
    private $hashKeyBackup;
    private $pathToConfig;

    public function __construct($params)
    {
        $this->pathToConfig = getcwd() . '/App/Config/';
        parent::__construct($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    protected function rules()
    {
        $filename = $this->pathToConfig . 'app.php';
        if (!file_exists($filename)) {

            throw new BootstrapException("[red]Erro[end]\n"
                . "[yellow]Não foi possível localizar o arquivo [end] "
                . "[blue]" . $filename . "[end]");
        }
    }

    public function run()
    {
        $filePath = $this->pathToConfig . 'app.php';
        $content = $this->prepareContent();
        if ($this->createFile($filePath, $content)) {
            $this->showMsg("[green]Sucesso ao mudar a chave da aplicação [end][blue]" . $filePath . "[end]");
        }

        $filePath = $this->pathToConfig .date("Y-m-d-H-i-s", time()) . '_key.txt';
        $content = $this->extractOldKey();
        if ($this->createFile($filePath, $content)) {
            $this->showMsg("[green]Sucesso ao criar o backup da chave antiga em: [end][blue]" . $filePath . "[end]");
        }
    }

    protected function prepareContent()
    {
        $filePath = $this->pathToConfig . 'app.php';
        $fileContent = file($filePath);
        $content = null;

        foreach ($fileContent as $value) {

            if (strstr($value, "define('STRSAL',")) {
                $content .= $this->createSalt();
                $this->hashKeyBackup = $value;
                continue;
            }

            $content .= $value;
        }

        return $content;
    }

    private function createSalt()
    {
        $this->hashKey = sha1(time() . md5(APP_NAME . APP_VERSION . time()));

        if (array_key_exists('key', $this->params)) {
            $this->hashKey = $this->params['key'];
        }

        return 'define(\'STRSAL\', \'' . $this->hashKey . '\');' . "\n";
    }

    private function extractOldKey()
    {
        // separando apenas a string usada com chave
        $oldKey = explode("'", $this->hashKeyBackup);

        return $oldKey[3];
    }

    protected function getHeaderComment()
    {
        // todo
    }

    public function getUses()
    {
        // todo
    }
}
