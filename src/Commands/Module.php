<?php

namespace Assist\Commands;

use Assist\Commands\CommandAbstract;
use Assist\Interfaces\CommandInterface;
use Assist\Commands\Controller;
use Assist\Commands\Model;
use Assist\Commands\Validator;
use Assist\Commands\View;

/**
 * Comando responsável por criar os módulos do sistema (MVC)
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Module extends CommandAbstract implements CommandInterface
{

    private $contollerCommand;
    private $modelCommand;
    private $validatorCommand;
    private $viewCommand;

    public function __construct($params)
    {
        parent::__construct($params);

        $params[0] = 'controller';
        $this->contollerCommand = new Controller($params);

        $params[0] = 'model';
        $this->modelCommand = new Model($params);

        $params[0] = 'validator';
        $this->validatorCommand = new Validator($params);

        $params[0] = 'view';
        $this->viewCommand = new View($params);
    }

    protected function defaultTemplateFile()
    {
        $this->templateFile = 'src/templates/blank.php';
    }

    public function run()
    {
        $this->contollerCommand->run();
        $this->modelCommand->run();
        $this->validatorCommand->run();
        $this->viewCommand->run();
    }

    protected function rules()
    {
        // todo
    }

    protected function getHeaderComment()
    {
        // todo
    }

    public function getUses()
    {
        // todo
    }
}
