<?php
[!]getHeaderComment

namespace App\Validators;

[!]getUses

trait [!]getName<=>['capitalize']
ValidatorTrait
{
    use \HTR\System\CommonTrait;

[!]makeValidates
}
