{
    "database" : {
        "sgbd" : "[!]getSgdb
",
        "host" : "[!]getHostName
",
        "dbname" : "[!]getDbName
",
        "username" : "[!]getUserName
",
        "password" : "[!]getPassword
"
    },
    "commands" : {
        "controller" : {
            "template" : "src/templates/controller.template.php"
        },
        "model" : {
            "template" : "src/templates/model.template.php"
        },
        "validator" : {
            "template" : "src/templates/validator.template.php"
        },
        "vwindex" : {
            "template" : "src/templates/view_index.template.php"
        },
        "vwinsert" : {
            "template" : "src/templates/view_form_novo.template.php"
        },
        "vwupdate" : {
            "template" : "src/templates/view_form_editar.template.php"
        }
    }
}
