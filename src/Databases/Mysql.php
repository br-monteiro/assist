<?php

namespace Assist\Databases;

use Assist\Exceptions\DatabaseException;
use Assist\Databases\SgbdAbstract;

/**
 * Responsável por prover as abstrações e conexões com banco de dados MySQL
 *
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class Mysql extends SgbdAbstract
{
    private $host;
    private $dbname;
    private $username;
    private $password;

    public function __construct(\stdClass $value)
    {
        $this->attribConnect = $value;

        $this->rules()
            ->connect();
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getDbname()
    {
        return $this->dbname;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
        return $this;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Conecta com o Banco de Dados MySQL
     * @return \PDO
     * @throws DatabaseException
     */
    protected function connect()
    {
        try {
            // realiza a aconexão com MySQL através do PDO
            $pdo = new \PDO(
                'mysql:host=' . $this->getHost()
                . ';dbname=' . $this->getDbname(), $this->getUsername(), $this->getPassword(),
                [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
            );

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $this->connect = $pdo;

            // Retorna a referência do objeto PDO
            return $pdo;
        } catch (\PDOException $e) {

            throw new DatabaseException("[red]Erro\n"
                . "Não foi possível conectar com o Banco de Dados.[end]\n"
                . "Código: [yellow]" . $e->getCode() . "[end]\n"
                . "Mensagem: [yellow]" . $e->getMessage() . "[end]");
        }
    }

    /**
     * Verifica se as regras de conexão com banco foram atendidas
     * @throws DatabaseException
     */
    protected function rules()
    {
        // verifica o host
        if (!isset($this->attribConnect->database->host)) {
            throw new DatabaseException("[red]Erro[end]\n"
            . "[yellow]Não foi possível conectar ao DB."
            . " É necessário fornecer o [end][blue]host[end]\n"
            . "[yellow]Verifique as configurações no arquivo htr.json[end]");
        }

        // verifica o nome do banco
        if (!isset($this->attribConnect->database->dbname)) {
            throw new DatabaseException("[red]Erro[end]\n"
            . "[yellow]Não foi possível conectar ao DB."
            . " É necessário fornecer o [end][blue]dbname[end]\n"
            . "[yellow]Verifique as configurações no arquivo htr.json[end]");
        }

        // verifica o usuário
        if (!isset($this->attribConnect->database->username)) {
            throw new DatabaseException("[red]Erro[end]\n"
            . "[yellow]Não foi possível conectar ao DB."
            . " É necessário fornecer o [end][blue]username[end]\n"
            . "[yellow]Verifique as configurações no arquivo htr.json[end]");
        }

        // verifica a senha
        if (!isset($this->attribConnect->database->password)) {
            throw new DatabaseException("[red]Erro[end]\n"
            . "[yellow]Não foi possível conectar ao DB."
            . " É necessário fornecer o [end][blue]password[end]\n"
            . "[yellow]Verifique as configurações no arquivo htr.json[end]");
        }


        return $this->setAllValues();
    }

    /**
     * Seta todos os valores de uma vez
     * @return \Assist\Databases\Mysql
     */
    private function setAllValues()
    {
        $attrib = $this->attribConnect->database;

        return $this->setHost($attrib->host)
                ->setDbname($attrib->dbname)
                ->setUsername($attrib->username)
                ->setPassword($attrib->password);
    }

    /**
     * Abstrai os dados da tabela
     * @throws DatabaseException
     */
    public function makeDescription()
    {
        try {

            $dbname = $this->attribConnect->database->dbname;

            $this->verifyEntity($this->getEntity());

            $stmt = $this->getConnect()->prepare("SELECT COLUMN_NAME, DATA_TYPE, COLUMN_TYPE, "
                . "CHARACTER_MAXIMUM_LENGTH, IS_NULLABLE, COLUMN_KEY, COLUMN_COMMENT "
                . "FROM information_schema.COLUMNS "
                . "WHERE TABLE_SCHEMA = '{$dbname}' "
                . "AND TABLE_NAME = '" . $this->getEntity() . "';");

            $stmt->execute();
            $arrDesc = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            for ($i = 0; $i < count($arrDesc); $i++) {

                $length = $arrDesc[$i]['CHARACTER_MAXIMUM_LENGTH'] ? : $this->abstractLength($arrDesc[$i]['COLUMN_TYPE']);

                $this->descriptionTable[$i]['field'] = $arrDesc[$i]['COLUMN_NAME'];
                $this->descriptionTable[$i]['type'] = $arrDesc[$i]['DATA_TYPE'];
                $this->descriptionTable[$i]['length'] = $length;
                $this->descriptionTable[$i]['is_null'] = ($arrDesc[$i]['IS_NULLABLE'] == 'YES');
                $this->descriptionTable[$i]['is_primary'] = ($arrDesc[$i]['COLUMN_KEY'] == 'PRI');
                $this->descriptionTable[$i]['comment'] = $arrDesc[$i]['COLUMN_COMMENT'];
            }
        } catch (\PDOException $e) {

            throw new DatabaseException("[red]Erro\n"
                . "Não foi possível abstrair as informações do Bando de Dados.[end]\n"
                . "Código: [yellow]" . $e->getCode() . "[end]\n"
                . "Mensagem: [yellow]" . $e->getMessage() . "[end]");
        }
    }

    /**
     * Abstrai as referências das relações das tabelas
     * @return null
     * @throws DatabaseException
     */
    public function makeReference()
    {
        try {
            $dbname = $this->attribConnect->database->dbname;

            $this->verifyEntity($this->getEntity());

            $stmt = $this->getConnect()->prepare("SELECT COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME "
                . "FROM information_schema.KEY_COLUMN_USAGE "
                . "WHERE TABLE_SCHEMA = '{$dbname}' "
                . "AND TABLE_NAME = '" . $this->getEntity() . "' "
                . "AND REFERENCED_TABLE_NAME IS NOT NULL;");

            $stmt->execute();
            $arrRef = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($arrRef) == 0) {
                $this->referenceTable[0] = $this->modelReferenceTable;
                return;
            }

            for ($i = 0; $i < count($arrRef); $i++) {

                $this->referenceTable[$i]['field'] = $arrRef[$i]['COLUMN_NAME'];
                $this->referenceTable[$i]['reference_table'] = $arrRef[$i]['REFERENCED_TABLE_NAME'];
                $this->referenceTable[$i]['reference_field'] = $arrRef[$i]['REFERENCED_COLUMN_NAME'];

            }
        } catch (\PDOException $e) {

            throw new DatabaseException("[red]Erro\n"
                . "Não foi possível abstrair os relacionamentos da tabelas.[end]\n"
                . "Código: [yellow]" . $e->getCode() . "[end]\n"
                . "Mensagem: [yellow]" . $e->getMessage() . "[end]");
        }
    }

    protected function verifyEntity($entity)
    {
        $dbname = $this->attribConnect->database->dbname;
        $stmt = $this->getConnect()->prepare("SELECT table_name FROM
            information_schema.tables WHERE table_schema = ? AND table_name = ?");
        $stmt->execute([$dbname, $entity]);

        if ($stmt->rowCount() == 0) {

            throw  new DatabaseException("[red]Erro[end]:\n"
                . "[yellow]Não foi possível localizar a entidade em [end][red]" .$dbname . "." . $entity . "[end].");
        }
    }

}
