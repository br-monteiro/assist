<?php

namespace Assist\Databases;

use Assist\System\LoadJsonFile;
use Assist\Exceptions\DatabaseException;

/**
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
class SgbdFactory
{

    private $sgbd;

    public function __construct()
    {
        $json = $this->loadConfigFile();
        $this->verifySgbdLibExists($json);
        return $this->getSgbd();
    }

    /**
     * Carrega o arquivo de configuração
     * @return \stdClass
     * @throws DatabaseException
     */
    private function loadConfigFile()
    {
        // carrega o arquivo de configuração htr.json
        $json = new LoadJsonFile();
        $json = $json->getJson();

        // verifica se o arquivo tem a diretiva indicando o SGDB usado
        if (!isset($json->database->sgbd)) {
            throw new DatabaseException("[red]Erro[end]\n"
            . "[yellow]Não foi encontrada a diretiva de identificação do SGDB.[end]\n"
            . "[yellow]Verifique o arquivo htr.json.[end]");
        }

        return $json;
    }

    /**
     * Verifica se há uma uma biblioteca para o SGDB especificado
     * @param \stdClass $json Atributos carregados do arquivo Json
     * @return null
     * @throws DatabaseException
     */
    private function verifySgbdLibExists($json)
    {
        $class_name = "\\Assist\\Databases\\" . ucfirst(strtolower($json->database->sgbd));

        if (class_exists($class_name)) {

            $class_name = new $class_name($json);

            if (is_a($class_name, SgbdAbstract::class)) {
                $this->sgbd = $class_name;
            }

            // para a execução do método
            return;
        }

        throw new DatabaseException("[red]Erro[end]\n"
        . "[red]{$json->database->sgbd}[end]: [yellow]Este SGBD não é reconhecido pelo sistema.[end]");
    }

    public function getSgbd()
    {
        return $this->sgbd;
    }
}
