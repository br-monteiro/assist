<?php

namespace Assist\Databases;

use Assist\Exceptions\DatabaseException;

/**
 * @author Bruno Monteiro <bruno.monteirodg@gmail.com>
 */
abstract class SgbdAbstract
{
    /**
     * Modelo de índices que o atributo $descriptionTables deve possuir
     * @var array
     */
    private $modelOfDescriptioTable = [
        'field' => null,
        'type' => null,
        'length' => null,
        'is_null' => null,
        'is_primary' => null,
        'comment' => null
    ];

    /**
     * Modelo de índices que o atributo $descriptionTable deve possuir
     * @var array
     */
    protected $modelReferenceTable = [
        'field' => null,
        'reference_table' => null,
        'reference_field' => null
    ];

    protected $descriptionTable = [];

    protected $referenceTable = [];

    protected $attribConnect;

    protected $connect;

    protected $entity;

    abstract public function makeDescription();

    abstract public function makeReference();

    abstract protected function rules();

    abstract protected function connect();

    abstract protected function verifyEntity($entity);

    public function getConnect()
    {
        return $this->connect;
    }

    public function getAttribConnect()
    {
        return $this->attribConnect;
    }

    public function getDescriptionTable()
    {
        $this->checkCompatibilityOfArraysDescription();

        return $this->descriptionTable;
    }

    public function getReferenceTable()
    {
        $this->checkCompatibilityOfArraysReference();

        return $this->referenceTable;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Verifica se os índices do array gerado pelo metodo makeDescriptioTable
     * são iguais a do atributo modelOfDescriptioTable
     * @throws DatabaseException
     */
    private function checkCompatibilityOfArraysDescription()
    {
        $diffKeys1 = array_diff_key($this->descriptionTable[0], $this->modelOfDescriptioTable);
        $diffKeys2 = array_diff_key($this->modelOfDescriptioTable, $this->descriptionTable[0]);

        $diffKeys1 = array_merge($diffKeys1, $diffKeys2);

        if (count($diffKeys1) > 0) {

            throw new DatabaseException("[red]Erro[end]\n"
                . "[yellow]A class usada para abstração de informações do SGBD não implementa\n"
                . "corretamente os índices do array [blue]descriptionTable[end].\n"
                . "[yellow]Diferença de índices:[end] [red]" . implode(", ", array_keys($diffKeys1)) . "[end]");
        }

    }

    /**
     * Verifica se os índices do array gerado pelo metodo makeReferenceTable
     * são iguais a do atributo modelReferenceTable
     * @throws DatabaseException
     */
    private function checkCompatibilityOfArraysReference()
    {
        $diffKeys1 = array_diff_key($this->referenceTable[0], $this->modelReferenceTable);
        $diffKeys2 = array_diff_key($this->modelReferenceTable, $this->referenceTable[0]);

        $diffKeys1 = array_merge($diffKeys1, $diffKeys2);

        if (count($diffKeys1) > 0) {

            throw new DatabaseException("[red]Erro[end]\n"
                . "[yellow]A class usada para abstração das relações do SGBD não implementa\n"
                . "corretamente os índices do array [blue]referenceTable[end].\n"
                . "[yellow]Diferença de índices:[end] [red]" . implode(", ", array_keys($diffKeys1)) . "[end]");
        }

        // verifica se o array possui apenas o modelo como valores,
        // ou seja, não há relação com nenhuma tabela
        if ($this->referenceTable[0]['field'] == null) {
            $this->referenceTable = [];
        }
    }

    /**
     * Abstrai o tamanho do campo da tabela.
     * @param string $fieldType Tipo do campo a ser processado
     * @return int Retorna o tamanho do campo informado
     */
    protected function abstractLength($fieldType)
    {
        $fieldType = explode("(", $fieldType);
        $fieldType =  str_replace(")", "", $fieldType);

        return isset($fieldType[1]) ? $fieldType[1] : 0;
    }
}
