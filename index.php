<?php

/**
 * Assistente de linha de comando do HTR Framework
 * @version 2.0
 * @author Bruno Monteiro <bruno.monteirodg@gail.com> | Paulo H Gaia <phcgaia11@yahoo.com.br> 
 */

require_once 'vendor/autoload.php';
require_once 'src/Config/app.php';

/**
 * Tenta iniciar a aplicação
 */
try {
    new Assist\Bootstrap($argv);
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}
